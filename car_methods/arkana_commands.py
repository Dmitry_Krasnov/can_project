import time
import can

# -------------------------------------
#           variables
# -------------------------------------
arkana_msg = can.Message(arbitration_id=0x55D,
                        data=[0x07, 0x1D, 0xF4, 0x0F, 0x90, 0xC0, 0x20, 0x80],
                        is_extended_id=False)


# ---------------------------------------------
# List of commands for Mitsubishi
# ---------------------------------------------
class Arkana:

    @staticmethod
    def can_bitrate():  # Arkana CAN Bitrate
        return 500000

    @staticmethod
    def voice(bus):  # Voice
        can.Message.arbitration_id = 0x3B7
        can.Message.dlc = 8
        can.Message.data = [0x39, 0x7D, 0xBD, 0x00, 0x00, 0x41, 0x39, 0x00]
        bus.send(arkana_msg)
        time.sleep(0.01)
        can.Message.data = [0x39, 0x7D, 0xBD, 0x00, 0x00, 0x41, 0x39, 0x00]
        bus.send(arkana_msg)
        time.sleep(0.01)
        can.Message.data = [0x39, 0x7D, 0xBD, 0x00, 0x00, 0x40, 0x39, 0x00]
        bus.send(arkana_msg)
        time.sleep(0.01)

    @staticmethod
    def wake_up(bus):  # Wake_up
        can.Message.arbitration_id = 0x55D
        can.Message.dlc = 8
        can.Message.data = [0x07, 0x1D, 0xF4, 0x0F, 0x90, 0xC0, 0x20, 0x80]
        bus.send(arkana_msg)
        time.sleep(0.01)
        can.Message.data = [0x07, 0x1D, 0xF4, 0x0F, 0x90, 0xC0, 0x20, 0x80]
        bus.send(arkana_msg)
        time.sleep(0.01)
        can.Message.arbitration_id = 0x350
        can.Message.dlc = 8
        can.Message.data = [0xC7, 0x0B, 0x5F, 0xD9, 0x14, 0x98, 0xA4, 0x05]
        bus.send(arkana_msg)
        time.sleep(0.01)
        can.Message.data = [0xC7, 0x0B, 0x5F, 0xD9, 0x14, 0x98, 0xA4, 0x05]
        bus.send(arkana_msg)
        time.sleep(0.01)

    @staticmethod
    def handbrake(bus):  # Handbrake (unblock AA)
        can.Message.arbitration_id = 0x4F8
        can.Message.dlc = 8
        can.Message.data = [0x58, 0x00, 0x10, 0x00, 0x00, 0x08, 0x00, 0x00]
        bus.send(arkana_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x58, 0x00, 0x10, 0x00, 0x00, 0x08, 0x00, 0x00]
        bus.send(arkana_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
