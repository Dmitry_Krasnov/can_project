import time
import can

# -------------------------------------
#           variables
# -------------------------------------
vesta_msg = can.Message(arbitration_id=0x315,
                       data=[0, 64, 0, 0, 0, 0, 0, 0],
                       is_extended_id=False)


# ---------------------------------------------
# List of commands for Vesta
# ---------------------------------------------
class Vesta:

    @staticmethod
    def can_bitrate():  # Vesta CAN Bitrate
        return 500000

    @staticmethod
    def handbrake(bus):  # Handbrake (start AA)
        can.Message.arbitration_id = 0x4F8
        can.Message.dlc = 8
        can.Message.data = [0x58, 0x00, 0x10, 0x00, 0x00, 0x08, 0x00, 0x00]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x58, 0x00, 0x10, 0x00, 0x00, 0x08, 0x00, 0x00]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def Speed_5_km_h(bus):  # Speed about 5 km/h
        can.Message.arbitration_id = 0x5D7
        can.Message.dlc = 7
        can.Message.data = [0x00, 0x00, 0x00, 0x57, 0xA5, 0x70, 0xCC]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x00, 0x00, 0x00, 0x57, 0xA5, 0x70, 0xCC]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def Speed_15_km_h(bus):  # Speed about 15 km/h
        can.Message.arbitration_id = 0x5D7
        can.Message.dlc = 7
        can.Message.data = [0x05, 0x18, 0x00, 0x57, 0xA5, 0x80, 0xD8]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x05, 0x18, 0x00, 0x57, 0xA5, 0x80, 0xD8]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def Reversing_on(bus):  # Reversing ON
        can.Message.arbitration_id = 0x55D
        can.Message.dlc = 8
        can.Message.data = [0x00, 0xED, 0x60, 0xC0, 0x92, 0x80, 0x00, 0x00]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x00, 0xED, 0x60, 0xC0, 0x92, 0x80, 0x00, 0x00]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)

    @staticmethod
    def Reversing_off(bus):  # Reversing OFF
        can.Message.arbitration_id = 0x55D
        can.Message.dlc = 8
        can.Message.data = [0x00, 0xDD, 0x60, 0xC0, 0x92, 0x80, 0x00, 0x00]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
        can.Message.data = [0x00, 0xDD, 0x60, 0xC0, 0x92, 0x80, 0x00, 0x00]
        bus.send(vesta_msg)  # Connect and send frame by CANable
        time.sleep(0.1)
