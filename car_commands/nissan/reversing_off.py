from can import Bus

from car_methods.base_constance import nissan_can_bitrate, bus_type
from car_methods.nissan_commands import Nissan
from serial_port_connection.connection_mode import auto_port_scan

"""

"""
bus = Bus(bustype=bus_type, channel=auto_port_scan(), bitrate=nissan_can_bitrate)

Nissan().reversing_off(bus)
print("Задняя камера выключена")
