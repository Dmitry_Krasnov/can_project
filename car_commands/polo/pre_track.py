from can import Bus

from car_methods.base_constance import polo_can_bitrate, bus_type
from car_methods.polo_commands import Polo
from serial_port_connection.connection_mode import auto_port_scan

"""

"""
bus = Bus(bustype=bus_type, channel=auto_port_scan(), bitrate=polo_can_bitrate)

Polo().pre_track(bus)
print("")