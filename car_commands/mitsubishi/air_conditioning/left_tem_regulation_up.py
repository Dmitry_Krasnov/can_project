from can import Bus

from car_methods.base_constance import mitsubishi_can_bitrate, bus_type
from car_methods.mitsubishi_commands import Mitsubishi
from serial_port_connection.connection_mode import auto_port_scan

"""
Поднятие уровня температуры обдува для водителя
"""
bus = Bus(bustype=bus_type, channel=auto_port_scan(), bitrate=mitsubishi_can_bitrate)

Mitsubishi().left_tem_regulation(bus)
print("Температура для вродителя увеличина")
