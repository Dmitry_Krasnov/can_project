from can import Bus

from car_methods.base_constance import mitsubishi_can_bitrate, bus_type
from car_methods.mitsubishi_commands import Mitsubishi
from serial_port_connection.connection_mode import auto_port_scan

"""
Включение кондиционера
"""
bus = Bus(bustype=bus_type, channel=auto_port_scan(), bitrate=mitsubishi_can_bitrate)

Mitsubishi().ac_on(bus)
print("Кондиционер включен")
