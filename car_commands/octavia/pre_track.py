from can import Bus

from car_methods.base_constance import octavia_can_bitrate, bus_type
from car_methods.octavia_rapid_commands import Octavia_Rapid
from serial_port_connection.connection_mode import auto_port_scan

"""

"""
bus = Bus(bustype=bus_type, channel=auto_port_scan(), bitrate=octavia_can_bitrate)

Octavia_Rapid().pre_track(bus)
print("")