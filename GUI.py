#!/usr/bin/python3
# -*- coding: utf-8 -*-

from can import Bus

from car_methods.base_constance import polo_can_bitrate, octavia_can_bitrate, nissan_can_bitrate, \
    mitsubishi_can_bitrate, bus_type, vesta_can_bitrate, granta_can_bitrate, arkana_can_bitrate

from car_methods.arkana_commands import Arkana
from car_methods.polo_commands import Polo
from car_methods.nissan_commands import Nissan
from car_methods.mitsubishi_commands import Mitsubishi
from car_methods.octavia_rapid_commands import Octavia_Rapid
from car_methods.vesta_commands import Vesta
from car_methods.granta_commands import Granta
from serial_port_connection.connection_mode import auto_port_scan
import can_parser.parser_for_can_hacker_files

import sys
import threading
import time

from PyQt5.QtWidgets import QApplication, QMainWindow, QAction, qApp, QPushButton, QComboBox, \
    QFileDialog, QLabel, QTextBrowser, QTabWidget, QMenuBar, QStatusBar
from PyQt5.QtCore import QCoreApplication
from PyQt5.QtGui import QIcon
from pathlib import Path


class MyThread(threading.Thread):

    def __init__(self, name):
        """Инициализация потока"""
        threading.Thread.__init__(self)
        self.name = name

    def run(self):
        """Запуск потока"""
        global arkana_flag, arkana_voice_command_flag, arkana_Handbrake_command_flag
        print("Thread Run")
        bus = Bus(bustype=bus_type, channel=auto_port_scan(), bitrate=arkana_can_bitrate)
        while 1 > 0:
            if arkana_flag == "on":
                Arkana().wake_up(bus)
                time.sleep(0.01)
                if arkana_voice_command_flag == "on":
                    Arkana().voice(bus)
                    arkana_voice_command_flag = "off"
                if arkana_Handbrake_command_flag == "on":
                    Arkana().handbrake(bus)
                    arkana_Handbrake_command_flag = "off"
            if thread_flag == "off":
                print("Thread OFF")
                break


class GUI_CAN_Adapter(QMainWindow):

    def __init__(self):
        super().__init__()

        self.myclose = True  # myclose = True - закроете X и остановится фоновый поток
        self.combo_box = QComboBox(self)
        self.consol = QTextBrowser(self)
        self.label_avto = QLabel('', self)
        self.label_combo_box = QLabel('Команда для отправки:', self)
        self.clear_button = QPushButton('Очистить', self)
        self.initUI()
        self.current_auto = ""  # Polo/Octavia/Nissan/Mitsu...
        self.directory = ""
        self.fileName = ""

    def initUI(self):

        # Start Status Bur
        self.statusBar()    # Add Status

        # Menu "Exit"
        # exitAct = QAction(QIcon('exit.png'), '&Exit', self)
        # exitAct.setShortcut('Ctrl+Q')
        # exitAct.setStatusTip('Exit application')
        # exitAct.triggered.connect(qApp.quit)
        # Menu "Open File"
        # open_file = QAction(QIcon('open.png'), 'Open CAN-Dump', self)
        # open_file.setShortcut('Ctrl+O')
        # open_file.setStatusTip('Open new File')
        # open_file.triggered.connect(self.showDialog)
        # Menu "Polo Car"
        polo_car = QAction('Polo commands', self)
        polo_car.setStatusTip('Polo commands')
        polo_car.triggered.connect(self.polo)
        # Menu "Octavia Car"
        octavia_car = QAction('Octavia commands', self)
        octavia_car.setStatusTip('Octavia commands')
        octavia_car.triggered.connect(self.octavia)
        # Menu "Nissan Car"
        nissan_car = QAction('Nissan commands', self)
        nissan_car.setStatusTip('Nissan commands')
        nissan_car.triggered.connect(self.nissan)
        # Menu "Mitsubishi Car"
        mitsubishi_car = QAction('Mitsubishi commands', self)
        mitsubishi_car.setStatusTip('Mitsubishi commands')
        mitsubishi_car.triggered.connect(self.mitsubishi)
        # Menu "Vesta & X-Ray Car"
        vesta_car = QAction('Vesta and X-Ray commands', self)
        vesta_car.setStatusTip('Vesta and X-Ray commands')
        vesta_car.triggered.connect(self.vesta)
        # Menu "Granta"
        granta_car = QAction('Granta commands', self)
        granta_car.setStatusTip('Granta commands')
        granta_car.triggered.connect(self.granta)
        arkana_car = QAction('Arkana commands', self)
        arkana_car.setStatusTip('Arkana commands')
        arkana_car.triggered.connect(self.arkana)
        # Can-Hacker CAN-Dump Play
        # logs_player = QAction('CAN-logs player', self)
        # logs_player.setStatusTip('CAN-logs player')
        # logs_player.triggered.connect(self.can_logs_player)
        # open_file = QAction(QIcon('open.png'), 'Open CAN-Dump', self)
        # open_file.setStatusTip('Open new File')
        # open_file.triggered.connect(self.showDialog)

        # Menu-Bar
        menubar = self.menuBar()
        # fileMenu = menubar.addMenu('&File')
        # fileMenu.addAction(exitAct)
        # fileMenu = menubar.addMenu('&Play CAN-Dump')
        # fileMenu.addAction(open_file)
        # fileMenu = menubar.addMenu('&Polo')
        # fileMenu.addAction(polo_car)
        fileMenu = menubar.addMenu('&Octavia')
        fileMenu.addAction(octavia_car)
        fileMenu = menubar.addMenu('&Nissan')
        fileMenu.addAction(nissan_car)
        fileMenu = menubar.addMenu('&Mitsubishi')
        fileMenu.addAction(mitsubishi_car)
        fileMenu = menubar.addMenu('&Lada')
        fileMenu.addAction(vesta_car)
        fileMenu.addAction(granta_car)
        fileMenu = menubar.addMenu('&Arkana')
        fileMenu.addAction(arkana_car)
        # fileMenu = menubar.addMenu('&Logs player')
        # fileMenu.addAction(logs_player)
        # fileMenu.addAction(open_file)

        # Combo - box
        self.combo_box.setGeometry(20, 100, 200, 30)
        self.combo_box.setStatusTip('Choose command')
        # self.combo_box.activated[str].connect(self.onActivated_box)

        # Text Browser
        self.consol.setGeometry(10, 150, 480, 240)

        # Label Avto
        self.label_avto.resize(self.label_avto.sizeHint())
        self.label_avto.move(20, 40)

        # Label Combo - box
        self.label_combo_box.resize(self.label_combo_box.sizeHint())
        self.label_combo_box.move(20, 80)

        # Clear button
        self.clear_button.clicked.connect(self.click_clear_button)
        self.clear_button.setStatusTip('Clear All')
        self.clear_button.setGeometry(250, 100, 200, 30)
        # self.clear_button.move(250, 100)
        self.clear_button.show()

        # Send button
        send_button = QPushButton('Отправить', self)
        send_button.clicked.connect(self.click_send_button)
        send_button.setStatusTip('Send CAN-command')
        send_button.setGeometry(250, 40, 200, 30)
        send_button.show()

        self.setGeometry(300, 300, 500, 410)  # (X-position,Y-position,X-size,Y-size)
        self.setWindowTitle('CAN Adapter')
        self.setWindowIcon(QIcon('icon.png'))   # Add Icon

        self.show()

    def closeEvent(self, event):
        global thread_flag
        if self.myclose:
            if thread_flag == "on":
                self.stop_thread_info()
                thread_flag = "off"  # Thread OFF
            else:
                print("Application closed")

    def polo(self):
        global thread_flag
        if thread_flag == "on":
            self.stop_thread_info()
            thread_flag = "off"  # Thread OFF
        self.combo_box.show()
        self.combo_box.clear()
        self.combo_box.addItem("Volume Up")
        self.combo_box.addItem("Volume Down")
        self.combo_box.addItem("Next Track")
        self.combo_box.addItem("Previous Track")
        self.combo_box.addItem("Phone")
        self.combo_box.addItem("Voice")
        self.combo_box.addItem("Mute")
        #
        self.label_avto.setText('Текущее Авто: Polo')
        self.label_avto.resize(self.label_avto.sizeHint())
        self.current_auto = "Polo"

    def octavia(self):
        global thread_flag
        if thread_flag == "on":
            self.stop_thread_info()
            thread_flag = "off"  # Thread OFF
        self.combo_box.show()
        self.combo_box.clear()
        self.combo_box.addItem("Volume Up")
        self.combo_box.addItem("Volume Down")
        self.combo_box.addItem("Next Track")
        self.combo_box.addItem("Previous Track")
        self.combo_box.addItem("Next Track long press")
        self.combo_box.addItem("Previous Track long press")
        self.combo_box.addItem("Phone")
        self.combo_box.addItem("Voice")
        self.combo_box.addItem("Mute")
        #
        self.label_avto.setText('Текущее Авто: Octavia')
        self.label_avto.resize(self.label_avto.sizeHint())
        self.current_auto = "Octavia"

    def nissan(self):
        global thread_flag
        if thread_flag == "on":
            self.stop_thread_info()
            thread_flag = "off"  # Thread OFF
        self.combo_box.clear()
        self.combo_box.addItem("Volume Up")
        self.combo_box.addItem("Volume Down")
        self.combo_box.addItem("Next Track")
        self.combo_box.addItem("Previous Track")
        # self.combo_box.addItem("Reversing On")
        # self.combo_box.addItem("Reversing Off")
        self.combo_box.addItem("Phone")
        self.combo_box.addItem("Hang Up")
        self.combo_box.addItem("Mode")
        #
        self.label_avto.setText('Текущее Авто: Nissan')
        self.label_avto.resize(self.label_avto.sizeHint())
        self.current_auto = "Nissan"

    def mitsubishi(self):
        global thread_flag
        if thread_flag == "on":
            self.stop_thread_info()
            thread_flag = "off"  # Thread OFF
        self.combo_box.clear()
        # Air Conditioning
        self.combo_box.addItem("AC On")
        self.combo_box.addItem("AC Off")
        self.combo_box.addItem("Activation Dual fun mode")
        self.combo_box.addItem("Fun Auto mode on")
        self.combo_box.addItem("Fun Auto mode off")
        self.combo_box.addItem("Left Tem. regulation up")
        self.combo_box.addItem("Right Tem. regulation up")
        self.combo_box.addItem("Ventilator Power up")
        # Fan Direction
        self.combo_box.addItem("Face")
        self.combo_box.addItem("Face & Foot")
        self.combo_box.addItem("Foot")
        self.combo_box.addItem("Foot & Windscreen")
        # Rear Window Heating
        self.combo_box.addItem("Rear Window Heating On")
        self.combo_box.addItem("Rear Window Heating Off")
        # Recirculation
        self.combo_box.addItem("Recirculation Cabin")
        self.combo_box.addItem("Recirculation Outside")
        # Reversing On/Off
        self.combo_box.addItem("Reversing On")
        self.combo_box.addItem("Reversing Off")
        # Windscreen Heating
        self.combo_box.addItem("Windscreen Heating On")
        self.combo_box.addItem("Windscreen Heating Off")
        #
        self.label_avto.setText('Текущее Авто: Mitsubishi')
        self.label_avto.resize(self.label_avto.sizeHint())
        self.current_auto = "Mitsubishi"

    def vesta(self):
        global thread_flag
        if thread_flag == "on":
            self.stop_thread_info()
            thread_flag = "off"  # Thread OFF
        #
        self.combo_box.clear()
        self.combo_box.addItem("Handbrake (Unblock AA)")
        self.combo_box.addItem("Speed_5_km_h (Unblock Browser)")
        self.combo_box.addItem("Speed_15_km_h (Block Browser)")
        self.combo_box.addItem("Reversing On (Camera On)")
        self.combo_box.addItem("Reversing Off (Camera Off)")
        #
        self.label_avto.setText('Текущее Авто: Vesta & X-Ray')
        self.label_avto.resize(self.label_avto.sizeHint())
        self.current_auto = "Vesta"

    def granta(self):
        global thread_flag
        if thread_flag == "on":
            self.stop_thread_info()
            thread_flag = "off"  # Thread OFF
        #
        self.combo_box.clear()
        self.combo_box.addItem("Handbrake (Unblock AA)")
        self.combo_box.addItem("Speed_5_km_h (Unblock Browser)")
        self.combo_box.addItem("Speed_15_km_h (Block Browser)")
        #
        self.label_avto.setText('Текущее Авто: Granta')
        self.label_avto.resize(self.label_avto.sizeHint())
        self.current_auto = "Granta"

    def arkana(self):
        global thread_flag
        global arkana_flag
        # Check COM-port Status
        channel = auto_port_scan()
        if channel == "":
            self.com_port_not_found_info()
        else:
            thread_flag = "on"  # Thread ON
            arkana_flag = "on"
            my_thread = MyThread("Arkana_thread")
            my_thread.start()
            self.start_thread_info()

        self.combo_box.clear()
        self.combo_box.addItem("Voice")
        self.combo_box.addItem("Handbrake (Unblock AA)")
        #
        self.label_avto.setText('Текущее Авто: Arkana')
        self.label_avto.resize(self.label_avto.sizeHint())
        self.current_auto = "Arkana"

    def can_logs_player(self):
        global thread_flag
        if thread_flag == "on":
            self.stop_thread_info()
            thread_flag = "off"  # Thread OFF
        self.label_avto.setText('Выберите log-файл...')
        self.label_avto.resize(self.label_avto.sizeHint())
        self.label_combo_box.setText('Выберите скорость CAN: ')
        self.label_combo_box.resize(self.label_combo_box.sizeHint())
        self.combo_box.clear()
        self.combo_box.addItem("250000 кбит/с")
        self.combo_box.addItem("500000 кбит/с")
        self.combo_box.addItem("1000000 кбит/с")
        self.current_auto = "Logs Player"

    def showDialog(self):  # "Open File" Dialog, select file for open
        home_dir = str(Path.home())
        # self.fileName = QFileDialog.getOpenFileName(self, 'Open file', home_dir)
        self.fileName = QFileDialog.getSaveFileName(self, 'Open file', home_dir)

        # Add command to Info (TextBrowser)
        self.consol.append("-----------------------")
        self.consol.append('Log-файл: ' + str(self.fileName))
        self.consol.append("-----------------------")
        self.consol.repaint()
        print(str(self.fileName))

        # if fname[0]:
        #     f = open(fname[0], 'r')
        #
        #     with f:
        #         data = f.read()
        #         self.consol.setText(data)
        #         # self.textEdit.setText(data)

    def onActivated_box(self, text):
        # self.consol.setText(text)  # Replace old text to new one
        self.consol.append(text)    # Add new text to old
        self.consol.repaint()

    def click_send_button(self):
        global arkana_flag, arkana_voice_command_flag, arkana_Handbrake_command_flag
        command = self.combo_box.currentText()  # Current item from combo box
        current_auto = self.current_auto

        # Check COM-port Status
        curent_channel = auto_port_scan()
        if curent_channel == "":
            self.com_port_not_found_info()
        else:
            # Send commands
            if current_auto == "Polo":
                bus = Bus(bustype=bus_type, channel=curent_channel, bitrate=polo_can_bitrate)
                if command == "Volume Up":
                    Polo().volume_up(bus)
                    Polo().volume_up(bus)
                if command == "Volume Down":
                    Polo().volume_down(bus)
                    Polo().volume_down(bus)
                if command == "Next Track":
                    Polo().next_track(bus)
                    Polo().next_track(bus)
                if command == "Previous Track":
                    Polo().pre_track(bus)
                    Polo().pre_track(bus)
                if command == "Phone":
                    Polo().phone(bus)
                    Polo().phone(bus)
                if command == "Voice":
                    Polo().voice(bus)
                    Polo().voice(bus)
                if command == "Mute":
                    Polo().mute(bus)
                    Polo().mute(bus)

            if current_auto == "Octavia":
                bus = Bus(bustype=bus_type, channel=curent_channel, bitrate=octavia_can_bitrate)
                if command == "Volume Up":
                    Octavia_Rapid().volume_up(bus)
                    Octavia_Rapid().volume_up(bus)
                if command == "Volume Down":
                    Octavia_Rapid().volume_down(bus)
                    Octavia_Rapid().volume_down(bus)
                if command == "Next Track":
                    Octavia_Rapid().next_track(bus)
                    Octavia_Rapid().next_track(bus)
                if command == "Previous Track":
                    Octavia_Rapid().pre_track(bus)
                    Octavia_Rapid().pre_track(bus)
                if command == "Next Track long press":
                    Octavia_Rapid().next_long_press(bus)
                    Octavia_Rapid().next_long_press(bus)
                if command == "Previous Track long press":
                    Octavia_Rapid().pre_long_press(bus)
                    Octavia_Rapid().pre_long_press(bus)
                if command == "Phone":
                    Octavia_Rapid().phone(bus)
                    Octavia_Rapid().phone(bus)
                if command == "Voice":
                    Octavia_Rapid().voice(bus)
                    Octavia_Rapid().voice(bus)
                if command == "Mute":
                    Octavia_Rapid().mute(bus)
                    Octavia_Rapid().mute(bus)

            if current_auto == "Nissan":
                bus = Bus(bustype=bus_type, channel=curent_channel, bitrate=nissan_can_bitrate)
                if command == "Volume Up":
                    Nissan().volume_up(bus)
                    Nissan().volume_up(bus)
                if command == "Volume Down":
                    Nissan().volume_down(bus)
                    Nissan().volume_down(bus)
                if command == "Next Track":
                    Nissan().next_track(bus)
                    Nissan().next_track(bus)
                if command == "Previous Track":
                    Nissan().pre_track(bus)
                    Nissan().pre_track(bus)
                # if command == "Reversing On":
                #     Nissan().reversing_on(bus)
                #     Nissan().reversing_on(bus)
                # if command == "Reversing Off":
                #     Nissan().reversing_off(bus)
                #     Nissan().reversing_off(bus)
                if command == "Phone":
                    Nissan().phone(bus)
                    Nissan().phone(bus)
                if command == "Hang Up":
                    Nissan().hang_up(bus)
                    Nissan().hang_up(bus)
                if command == "Mode":
                    Nissan().mode(bus)
                    Nissan().mode(bus)

            if current_auto == "Mitsubishi":
                bus = Bus(bustype=bus_type, channel=curent_channel, bitrate=mitsubishi_can_bitrate)
                # Air Conditioning
                if command == "AC On":
                    Mitsubishi().ac_on(bus)
                if command == "AC Off":
                    Mitsubishi().ac_off(bus)
                if command == "Activation Dual fun mode":
                    Mitsubishi().dual(bus)
                if command == "Fun Auto mode on":
                    Mitsubishi().auto_on(bus)
                if command == "Fun Auto mode off":
                    Mitsubishi().auto_off(bus)
                if command == "Left Tem. regulation up":
                    Mitsubishi().left_tem_regulation(bus)
                if command == "Right Tem. regulation up":
                    Mitsubishi().right_tem_regulation(bus)
                if command == "Ventilator Power up":
                    Mitsubishi().ventilator_power(bus)
                # Fan Direction
                if command == "Face":
                    Mitsubishi().face(bus)
                if command == "Face & Foot":
                    Mitsubishi().face_foot(bus)
                if command == "Foot":
                    Mitsubishi().foot(bus)
                if command == "Foot & Windscreen":
                    Mitsubishi().foot_windscreen(bus)
                # Rear Window Heating
                if command == "Rear Window Heating On":
                    Mitsubishi().rear_window_heating_on(bus)
                if command == "Rear Window Heating Off":
                    Mitsubishi().rear_window_heating_off(bus)
                # Recirculation
                if command == "Recirculation Cabin":
                    Mitsubishi().recirculation_cabin(bus)
                if command == "Recirculation Outside":
                    Mitsubishi().recirculation_outside(bus)
                # Reversing On/Off
                if command == "Reversing On":
                    Mitsubishi().reversing_on(bus)
                if command == "Reversing Off":
                    Mitsubishi().reversing_off(bus)
                # Windscreen Heating
                if command == "Windscreen Heating On":
                    Mitsubishi().windscreen_heating_on(bus)
                if command == "Windscreen Heating Off":
                    Mitsubishi().windscreen_heating_off(bus)

            if current_auto == "Vesta":
                bus = Bus(bustype=bus_type, channel=curent_channel, bitrate=vesta_can_bitrate)
                if command == "Handbrake (Unblock AA)":
                    Vesta().handbrake(bus)
                if command == "Speed_5_km_h (Unblock Browser)":
                    Vesta().Speed_5_km_h(bus)
                if command == "Speed_15_km_h (Block Browser)":
                    Vesta().Speed_15_km_h(bus)
                if command == "Reversing On (Camera On)":
                    Vesta().Reversing_on(bus)
                if command == "Reversing Off (Camera Off)":
                    Vesta().Reversing_off(bus)

            if current_auto == "Granta":
                bus = Bus(bustype=bus_type, channel=curent_channel, bitrate=granta_can_bitrate)
                if command == "Handbrake (Unblock AA)":
                    Granta().handbrake(bus)
                if command == "Speed_5_km_h (Unblock Browser)":
                    Granta().Speed_5_km_h(bus)
                if command == "Speed_15_km_h (Block Browser)":
                    Granta().Speed_15_km_h(bus)

            if current_auto == "Arkana":
                if command == "Voice":
                    arkana_voice_command_flag = "on"
                if command == "Handbrake (Unblock AA)":
                    arkana_Handbrake_command_flag = "on"

            if current_auto == "Logs Player":
                # Bitrate
                if command == "250000 кбит/с":
                    bitrate = 250000
                if command == "500000 кбит/с":
                    bitrate = 500000
                if command == "1000000 кбит/с":
                    bitrate = 1000000
                bus = Bus(bustype=bus_type, channel=curent_channel, bitrate=bitrate)
                can_parser.parser_for_can_hacker_files.parser(self.fileName, bus)

            # Add command to Info (TextBrowser)
            if current_auto == "":
                self.consol.append("")
                self.consol.repaint()
            else:
                # Add command to Info (TextBrowser)
                self.consol.append("-----------------------")
                self.consol.append("Авто: " + current_auto)
                self.consol.append("Команда: " + command + " - отправлена")
                self.consol.append("-----------------------")
                self.consol.repaint()

    def click_clear_button(self):
        self.consol.clear()
        self.consol.repaint()

# Information messages:

    def stop_thread_info(self):
        # Add command to Info (TextBrowser)
        self.consol.append("---------------------------------")
        self.consol.append("Поток с командами запуска Arkana остановлен")
        self.consol.append("---------------------------------")
        self.consol.repaint()

    def com_port_not_found_info(self):
        # Add Info (TextBrowser)
        self.consol.append("---------------------------------")
        self.consol.append("COM port not found")
        self.consol.append("---------------------------------")
        self.consol.repaint()

    def start_thread_info(self):
        # Add test to Info (TextBrowser)
        self.consol.append("---------------------------------")
        self.consol.append("Запущен поток с командами запуска")
        self.consol.append("Дождитесь загрузки ГУ")
        self.consol.append("---------------------------------")
        self.consol.repaint()


if __name__ == '__main__':
    global arkana_flag, arkana_voice_command_flag, thread_flag
    thread_flag = "off"
    arkana_flag = "off"
    arkana_voice_command_flag = "off"
    arkana_Handbrake_command_flag = "off"

    app = QApplication(sys.argv)
    gui = GUI_CAN_Adapter()
    sys.exit(app.exec_())
