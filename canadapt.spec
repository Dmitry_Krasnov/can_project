# -*- mode: python ; coding: utf-8 -*-

block_cipher = None


a = Analysis(['GUI.py'],
             pathex=['/Users/vitz1337/Documents/PycharmProjects/can_project'],
             binaries=[],
             datas=[('icon.png', '.')],
             hiddenimports=['can.interfaces.slcan'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='canadapt',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=False , icon='moon.icns')
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               upx_exclude=[],
               name='canadapt')
app = BUNDLE(coll,
             name='canadapt.app',
             icon='moon.icns',
             bundle_identifier=None)
